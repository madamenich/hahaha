package com.metneak.repository.model;

import java.util.List;

public class Group {
    Integer id;
    Integer year;
    Integer school_id;
    List<User> users;
    School school;

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", year=" + year +
                ", school_id=" + school_id +
                ", users=" + users +
                ", school=" + school +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getSchool_id() {
        return school_id;
    }

    public void setSchool_id(Integer school_id) {
        this.school_id = school_id;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Group(){}
    public Group(Integer year, Integer school_id, List<User> users, School school) {
        this.year = year;
        this.school_id = school_id;
        this.users = users;
        this.school = school;
    }
}
