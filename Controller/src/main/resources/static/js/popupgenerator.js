document.getElementById("app_popup").innerHTML = `
<!-- Modal -->
<div class="modal fade" id="userPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <style>
      #cover {
          background-color:#eb4d4b;
          height: 216px;
      }
      .mt12 {
          margin-top: 12px;
      }
      .intro {
          position: relative;
          background-color: #7ed6df;
          margin: 0 auto;
          height: 300px;
      }
      .feed {
          background-color: #dff9fb;
          height: 300px;

      }
      #profile {
          height: 120px;
          width: 120px;
          background-color: #e056fd;
          position: absolute;
          left: 0;
          right: 0;
          top: -60px;
          margin: 0 auto;
          border-radius: 50%;
      }

      @media only screen and (max-width: 768px) {
          #profile {
              width: 150px;
              height: 150px;
              top: -75px;
          }
          .intro {
              height: unset;
          }
          .feed {
              margin-top: 85px;
          }
        }
  </style>
  <div class="modal-dialog" role="document">
          <div class="container ">
                  <div id="cover"></div>
                  <div class="row ">
              
                      <div class="col-12 ">
                        <div class="intro">
                              <div id="profile">
          
                                  </div>
                                <br><br><br>
                                <center>
                                    <h4><b> User name</b></h4>
                                </center>
                                <div class="mx-5 py-2">
                                    <h6><i class="fa fa-location-arrow mr-2" aria-hidden="true"></i>Intro</h6>
                                    <h6><i class="fa fa-university mr-2" aria-hidden="true"></i>Studied at HRD</h6>
                                    <h6><i class="fa fa-university mr-2" aria-hidden="true"></i>Studied at HRD</h6>
                                    <h6><i class="fa fa-university mr-2" aria-hidden="true"></i>Studied at HRD</h6>
                                    <h6><i class="fa fa-location-arrow mr-2" aria-hidden="true"></i>From Urgran</h6>
                                </div>
                        </div>
            </div>
          </div>
      </div>
  </div>
</div>
<div id="commentPopUp" class="modal fade" role="dialog">
  <style>
    .btn-radius{
      border-radius: 15px;
      width: 80%;
    }
    .user-profile-picture{
      border-radius: 50%;
      width: 50%;
      height: 40px;
    }
  </style>
  <div class="modal-dialog  modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <div class="container-fluid">
              <div class="row">
                <div class="col-3">
                  <span>
                    <img class="user-profile-picture" src="" alt="">
                  </span>
                </div>
                <div class="col-6"></div>
                <div class="col-3">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
              </div>
            </div>
        
      
      </div>
      <div class="modal-body">
        <div class="form-group">
          <textarea rows="8" class="form-control" id="message-text"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-3">
              <button class="btn btn-success btn-radius text-white">Cancel</button>
            </div>
            <div class="col-6"></div>
            <div class="col-3">
                <button class="btn btn-success btn-radius text-white">Save Change</button>
            </div>
          </div>
        </div>
    
      </div>
    </div>

  </div>
</div>

`;
$(".modal_btn").on('click',function(){
   let caption = $(this).parent().parent().children().eq(2).find("h5");
   $("#message-text").empty();
   $("#message-text").val(caption.text())

})