package com.metneak.service;

import com.metneak.repository.LikeRepository;
import com.metneak.repository.model.Like;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeService {
    @Autowired
    LikeRepository likeRepository;

    public boolean insertLike(Integer user_id, Integer post_id){
      return   likeRepository.insertLike(user_id, post_id);
    }

    public  boolean unlike(Integer user_id, Integer post_id){
        return  likeRepository.unlike(user_id,post_id);
    }
    public List<Like>getAllLike( Integer post_id){
        List<Like> allLike = likeRepository.getAllLike(post_id);
        return allLike;
    }

}
