package com.metneak.service;

import com.metneak.repository.GroupRepository;
import com.metneak.repository.SchoolRepository;
import com.metneak.repository.UserRepository;
import com.metneak.repository.model.Group;
import com.metneak.repository.model.School;
import com.metneak.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    UserRepository userRepository;
    public Boolean groupProcess(List<Group> groups, User user) {
        //            groupRepo.insertMember(user_id,group_id);
        List<Integer> all_group_id  = new ArrayList<>();

        groups.forEach(group -> {
           Integer group_id = insertGroup(group.getSchool_id(),group.getYear());
           all_group_id.add(group_id);
        });

        //  provide user 3 group_id
        user.setGroupBy(all_group_id.get(0),all_group_id.get(1),all_group_id.get(2));
        userRepository.addNewUser(user);

        // whenever it is UNIQUE it will absolute right
        User tem_user = userRepository.findUserByPhone(user.getPhone());

        all_group_id.forEach(group_id->{
            groupRepository.insertMember(tem_user.getId(),group_id);
        });

        return true;
    }
    /*
    * List<Group> groups
    * Everytime insert a new member, checking whether group (school , year) is created
    * if not , provide new group , then insert as first member
    * if has , directly insert as member
    * */
    public Integer insertGroup(Integer school_id,Integer year){
        try{
            if(groupRepository.ifGroupIsExist(year,school_id).size()>0){

            }else{
                groupRepository.insert(year,school_id);
            }

            Integer group_id = groupRepository.ifGroupIsExist(year,school_id).get(0).getId();

            return group_id;


        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }

    }


    @Autowired
    SchoolRepository schoolRepository;
    public List<School> allSchools(){
        return schoolRepository.findSchool();
    }

    public List<Group> findGroupDetail(Integer[] group) {
//        List<Group> groups = groupRepository.findGroups(group[0],group[1],group[2]);
        List<Group> groups = groupRepository.findGroups(1,2,3);
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        List<Group> groups = groupRepository.findGroups(list);
        return groups;
    }
}
